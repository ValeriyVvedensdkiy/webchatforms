﻿using System;
using System.Windows.Input;
using chat.Service.LocalStorage;
using chat.Views;
using Prism.Navigation;
using Xamarin.Forms;

namespace chat.ViewModels
{
    public class ConnectPageViewModel: ViewModelBase
    {
        #region Property
        private readonly INavigationService _navigationService;
        private readonly ILocalStorage _localStorage;
        public ICommand ConnectCommand { protected set; get; }
        private string _login;
        public string Login
        {
            get { return _login; }
            set { SetProperty(ref _login, value); }
        }
        #endregion

        public ConnectPageViewModel(INavigationService navigationService, ILocalStorage localStorage): base(navigationService)
        {
            _navigationService = navigationService;
            _localStorage = localStorage;
            ConnectCommand = new Command(ConnectionChat);
        }

        private void ConnectionChat()
        {
            _localStorage.Login = Login;
            _navigationService.NavigateAsync(nameof(ChatPage));
        }
    }
}
