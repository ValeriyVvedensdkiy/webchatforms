﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using chat.Models;
using chat.Service.LocalStorage;
using Newtonsoft.Json;
using Plugin.DeviceInfo;
using Prism.Navigation;
using Xamarin.Forms;

namespace chat.ViewModels
{
    public class ChatPageViewModel : ViewModelBase
    {
        #region Property

        readonly ClientWebSocket client;
        readonly CancellationTokenSource cts;

        public ICommand SendMessage { get; protected set; }
        private ObservableCollection<Message> _messages;
        public ObservableCollection<Message> Messages
        {
            get { return _messages; }
            set { SetProperty(ref _messages, value); }
        }
        private string _messageText;
        public string MessageText
        {
            get { return _messageText; }
            set { SetProperty(ref _messageText, value); }
        }
        private readonly INavigationService _navigationService;
        private readonly ILocalStorage _localStorage;
     
        #endregion
        public ChatPageViewModel(INavigationService navigationService, ILocalStorage localStorage) : base(navigationService)
        {
            _navigationService = navigationService;
            _localStorage = localStorage;
            client = new ClientWebSocket();
            cts = new CancellationTokenSource();
            _messages = new ObservableCollection<Message>();
            SendMessage  = new Command(SendMessageAsync);
            ConnectToServerAsync();
        }

        async Task ConnectToServerAsync()
        {

            // Uri from IOS
            //  await client.ConnectAsync(new Uri("ws://localhost:5000"), cts.Token);
            // Uri from Android Genimotion
               await client.ConnectAsync(new Uri("ws://10.0.3.2:5000"), cts.Token);
          
            await Task.Factory.StartNew(async () =>
            {
                while (true)
                {
                    WebSocketReceiveResult result;
                    var message = new ArraySegment<byte>(new byte[4096]);
                    do
                    {
                        result = await client.ReceiveAsync(message, cts.Token);
                        var messageBytes = message.Skip(message.Offset).Take(result.Count).ToArray();
                        string serialisedMessae = Encoding.UTF8.GetString(messageBytes);
                        try
                        {
                            var msg = JsonConvert.DeserializeObject<Message>(serialisedMessae);
                            Messages.Add(msg);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine($"Invalide message format. {ex.Message}");
                        }

                    } while (!result.EndOfMessage);
                }
            }, cts.Token, TaskCreationOptions.LongRunning, TaskScheduler.Default);

        }


        private async void SendMessageAsync()
        {
            var msg = new Message
            {
                Name = _localStorage.Login,
                MessagDateTime = DateTime.Now,
                Text = MessageText,
                UserId = CrossDeviceInfo.Current.Id
            };

            string serialisedMessage = JsonConvert.SerializeObject(msg);

            var byteMessage = Encoding.UTF8.GetBytes(serialisedMessage);
            var segmnet = new ArraySegment<byte>(byteMessage);

            await client.SendAsync(segmnet, WebSocketMessageType.Text, true, cts.Token);
            MessageText = string.Empty;
        }
    }
}
