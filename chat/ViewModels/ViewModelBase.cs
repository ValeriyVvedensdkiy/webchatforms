﻿using System;
using Prism.Mvvm;
using Prism.Navigation;

namespace chat.ViewModels
{
    public class ViewModelBase : BindableBase, INavigationAware
    {
        protected INavigationService _navigationService;

        public ViewModelBase(INavigationService navigationService)
        {
            _navigationService = navigationService;
        }

        public void OnNavigatedFrom(INavigationParameters parameters)
        {
           
        }

        public void OnNavigatedTo(INavigationParameters parameters)
        {
            
        }
    }
}
