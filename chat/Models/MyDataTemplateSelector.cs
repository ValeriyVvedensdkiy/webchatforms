﻿using System;
using chat.CustomCellMessage;
using Xamarin.Forms;

namespace chat.Models
{
    public class MyDataTemplateSelector: Xamarin.Forms.DataTemplateSelector
	{
		private readonly DataTemplate incomingDataTemplate;
		private readonly DataTemplate outgoingDataTemplate;

		public MyDataTemplateSelector()
		{
			// Retain instances!
			this.incomingDataTemplate = new DataTemplate(typeof(IncomingMsgCell));
			this.outgoingDataTemplate = new DataTemplate(typeof(OutgoingMsgCell));
		}

		protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
		{
			var messageVm = item as Message;
			if (messageVm == null)
				return null;
			return messageVm.IsIncoming ? this.incomingDataTemplate : this.outgoingDataTemplate;
		}

    
   
	}
}
