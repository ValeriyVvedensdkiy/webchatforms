﻿using System;
using Plugin.Settings.Abstractions;

namespace chat.Service.LocalStorage
{
    public class LocalStorage: ILocalStorage
    {
        private ISettings _appSettings;
        public LocalStorage(ISettings settings)
        {
            _appSettings = settings;
        }

        public string Login
        {
            get => _appSettings.GetValueOrDefault(nameof(Login), string.Empty);
            set => _appSettings.AddOrUpdateValue(nameof(Login), value);
        }
    }
}
